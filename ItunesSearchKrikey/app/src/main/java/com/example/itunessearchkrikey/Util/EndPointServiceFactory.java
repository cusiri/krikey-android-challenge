package com.example.itunessearchkrikey.Util;

import com.example.itunessearchkrikey.Model.ItunesInterface;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EndPointServiceFactory {


    public static ItunesInterface getInstance (){

        final String baseUrl = "https://itunes.apple.com/";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(ItunesInterface.class);
    }
}

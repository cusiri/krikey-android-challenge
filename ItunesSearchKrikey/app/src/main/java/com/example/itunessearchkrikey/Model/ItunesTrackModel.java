package com.example.itunessearchkrikey.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ItunesTrackModel {

    @SerializedName("resultCount")
    @Expose
    private Integer resultCount;

    @SerializedName("results")
    @Expose
    private ArrayList<Track> tracks = null;

    public Integer getResultCount() {
        return resultCount;
    }// Used to see if we actually got results from our query

    public ArrayList<Track> getTracks() {
        return tracks;
    }// Retrieve a list of track

}

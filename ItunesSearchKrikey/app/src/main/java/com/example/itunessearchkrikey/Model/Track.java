package com.example.itunessearchkrikey.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Track implements Serializable {

    // Implements Serializable interface
    @SerializedName("trackName")
    @Expose
    private String trackName;

    @SerializedName("artistName")
    @Expose
    private String artistName;

    @SerializedName("artworkUrl100")
    @Expose
    private String artworkUrl;

    @SerializedName("primaryGenreName")
    @Expose
    private String genreName;

    @SerializedName("previewUrl")
    @Expose
    private String previewUrl;

    @SerializedName("releaseDate")
    @Expose
    private String releaseDate;

    @SerializedName("trackPrice")
    @Expose
    private Double trackPrice;

    //Getters
    public String getTrackName() {
        return trackName;
    }

    public String getArtistName() {
        return artistName;
    }

    public Double getTrackPrice() {
        return trackPrice;
    }

    public String getArtworkUrl() {
        return artworkUrl;
    }

    public String getGenreName() {
        return genreName;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public String getReleaseDate() {
        String release = releaseDate.split("-")[0];
        return release;
    }
}

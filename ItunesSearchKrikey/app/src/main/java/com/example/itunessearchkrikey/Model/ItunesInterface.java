package com.example.itunessearchkrikey.Model;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface ItunesInterface {
    // GET call searching the Itunes API for the term that is provided in the search bar
    @GET("search")
    @Headers("Content-Type: application/x-www-form-urlencoded") // Request header requesting form encoding
    Call<ItunesTrackModel> queryForTracks(@Query("term") String term);
}

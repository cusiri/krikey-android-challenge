package com.example.itunessearchkrikey.Controllers.SearchFeedActivity;

import android.app.Activity;
import android.app.ActivityOptions;

import android.content.Context;
import android.content.Intent;

import android.support.v7.widget.RecyclerView;
import android.util.Pair;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.example.itunessearchkrikey.Controllers.TrackProfileActivity.TrackProfileMainActivity;
import com.example.itunessearchkrikey.Model.Track;
import com.example.itunessearchkrikey.R;
import com.example.itunessearchkrikey.Util.HideKeyboard;

import java.util.ArrayList;

public class SearchFeedRecyclerViewAdapter extends RecyclerView.Adapter<SearchFeedRecyclerViewAdapter.ViewHolder>{

    private Context mContext;
    private ArrayList<Track> trackList;

    public SearchFeedRecyclerViewAdapter(Context mContext, ArrayList<Track> trackList) {
        this.mContext = mContext;
        this.trackList = trackList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_search_feed_row_view,parent,false);
        //Create a view holder object, which is the class that we created
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.trackName.setText(trackList.get(position).getTrackName());
        holder.artistName.setText(trackList.get(position).getArtistName());
        holder.trackRelease.setText(trackList.get(position).getReleaseDate());

        //Download and cache image with glide
        Glide.with(mContext)
                .load(trackList.get(position)
                .getArtworkUrl())
                .placeholder(R.drawable.krikey_icon)
                .into(holder.artwork);
    }

    @Override
    public int getItemCount() {
        return trackList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private RelativeLayout mainLayout;

        private ImageView artwork;

        private TextView trackName;
        private TextView artistName;
        private TextView trackRelease;

        public ViewHolder(View itemView) {
            super(itemView);

            mainLayout = itemView.findViewById(R.id.layout_search_feed_card_view_main_layout_id);

            artwork = itemView.findViewById(R.id.layout_search_feed_image_id);

            trackName = itemView.findViewById(R.id.layout_search_feed_track_name_id);
            artistName = itemView.findViewById(R.id.layout_search_feed_artist_name);
            trackRelease = itemView.findViewById(R.id.layout_search_feed_track_release_date);

            handlerListeners();

        }

        private void handlerListeners(){

            // When a row is clicked
            mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Hide Keyboard when layout tapped
                    HideKeyboard hideKeyBoard = new HideKeyboard();
                    hideKeyBoard.setupUI(mainLayout,(Activity) mContext);

                    //Navigate to play track on track profile
                    Intent intent = new Intent(mContext, TrackProfileMainActivity.class);
                    intent.putExtra("track",trackList.get(getAdapterPosition()));// Get track that was selected

                    Pair[] pair = new Pair[3];
                    pair[0] = new Pair<View, String>(artistName,"artistTransition");
                    pair[1] = new Pair<View, String>(artwork,"imageTransition");
                    pair[2] = new Pair<View, String>(trackRelease,"releaseDateTransition");

                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext,pair);

                    mContext.startActivity(intent,options.toBundle());
                }
            });

        }
    }// Holds the widgets in memory

}

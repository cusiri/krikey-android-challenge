package com.example.itunessearchkrikey.Controllers.SearchFeedActivity;

import android.app.Activity;
import android.content.Context;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.os.Bundle;

import android.text.Editable;
import android.text.TextWatcher;

import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.itunessearchkrikey.Model.ItunesInterface;
import com.example.itunessearchkrikey.Model.ItunesTrackModel;
import com.example.itunessearchkrikey.Model.Track;
import com.example.itunessearchkrikey.R;
import com.example.itunessearchkrikey.Util.EndPointServiceFactory;

import com.example.itunessearchkrikey.Util.HideKeyboard;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchAndFeedMainActivity extends AppCompatActivity {
    private ArrayList<Track> tracksList;

    private RecyclerView recyclerView;

    private LinearLayout mainLayout;

    private EditText searchBarEditText;

    private final Context mContext = SearchAndFeedMainActivity.this;

    private SearchFeedRecyclerViewAdapter adapter;

    private static final String TAG = "SearchAndFeedMainActivi";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_feed_main);

        searchBarEditText = findViewById(R.id.search_feed_activity_main_edit_text_id);

        mainLayout = findViewById(R.id.search_feed_activity_main_layout_id);

        initRecyclerView();

        handlerListeners();

        // Hide Keyboard When View layout is tapped
        HideKeyboard hideKeyBoard = new HideKeyboard();
        hideKeyBoard.setupUI(mainLayout,(Activity) mContext);

    }

    //Handle OnClick Listeners
    private void handlerListeners(){

        //Listen For When Text Changes and query on each char
        searchBarEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                search(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    //Initialize RecyclerView
    private void initRecyclerView(){
        recyclerView = findViewById(R.id.search_feed_activity_recycler_view_id);
        tracksList = new ArrayList<>();
        adapter = new SearchFeedRecyclerViewAdapter(mContext,tracksList);//This is our default constructor that we made in our adapter
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);
    }

    private void search(String query){
        ItunesInterface service = EndPointServiceFactory.getInstance();

        Call<ItunesTrackModel> trackModelCall = service.queryForTracks(query);

        // Using Retrofit to create an interface, and operates in the background thread
        trackModelCall.enqueue(new Callback<ItunesTrackModel>() {
            @Override
            public void onResponse(Call<ItunesTrackModel> call, Response<ItunesTrackModel> response) {
                // Get the response
                ItunesTrackModel result = response.body();
                if(result !=null){
                    if (result.getResultCount() > 0) {
                        // Refresh adapter, we have tracks
                        tracksList.clear();
                        tracksList.addAll(result.getTracks());
                        adapter.notifyDataSetChanged(); // For optimization purposes, could use DiffUtil
                    }
                }
            }

            @Override
            public void onFailure(Call<ItunesTrackModel> call, Throwable t) {
                Toast.makeText(mContext, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });
    }
}

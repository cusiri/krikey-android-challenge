package com.example.itunessearchkrikey.Controllers.TrackProfileActivity;

import android.content.Context;

import android.graphics.drawable.AnimationDrawable;

import android.media.MediaPlayer;

import android.net.Uri;

import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;

import android.view.View;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.example.itunessearchkrikey.Model.Track;
import com.example.itunessearchkrikey.R;

public class TrackProfileMainActivity extends AppCompatActivity {
    private Track track;

    private TextView trackNameNavBar;
    private TextView trackArtist;
    private TextView trackGenre;
    private TextView trackPrice;
    private TextView trackReleaseDate;

    private Button backButton;

    private ImageView artwork;

    private AnimationDrawable animationDrawable;

    private RelativeLayout mainLayout;

    private MediaPlayer mediaPlayer;

    private final Context mContext = TrackProfileMainActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_profile_main);

        trackNameNavBar = findViewById(R.id.layout_nav_bar_title_id);
        trackArtist = findViewById(R.id.activity_track_profile_artist_name_detail);
        trackGenre = findViewById(R.id.activity_track_profile_genre_detail);
        trackPrice = findViewById(R.id.activity_track_profile_price_detail);
        trackReleaseDate = findViewById(R.id.activity_track_profile_release_date_detail);

        backButton = findViewById(R.id.layout_nav_bar_back_button_id);

        artwork = findViewById(R.id.activity_track_profile_image_id);

        mainLayout = findViewById(R.id.activity_track_profile_main_layout_id);

        animationDrawable = (AnimationDrawable) mainLayout.getBackground();

        handleListeners();
        getIntentExtras();
        setUpBackgroundAnimationScreen();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer.release();
        }
    }

    // Set up track information
    private void setupTrackInformation(Track track){
        trackNameNavBar.setText(getString(R.string.now_playing) +"\t"+track.getTrackName());
        trackArtist.setText(track.getArtistName());
        trackGenre.setText(track.getGenreName());
        trackPrice.setText(String.format("US $ %s", String.valueOf(track.getTrackPrice())));
        trackReleaseDate.setText(track.getReleaseDate());

        //Download and cache image with glide
        Glide.with(mContext)
                .load(track.getArtworkUrl())
                .placeholder(R.drawable.krikey_icon)
                .into(artwork);

        setUpMediaPlayer(track.getPreviewUrl());

    }

    // Animate background
    private void setUpBackgroundAnimationScreen() {
        if(animationDrawable != null){
            animationDrawable.setEnterFadeDuration(3000);
            animationDrawable.setExitFadeDuration(3000);
            animationDrawable.start();
        }
    }

    // handle Listerners
    private void handleListeners(){
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    //Get intent if exists
    private void getIntentExtras(){
        // Get track extra
        if(getIntent().getSerializableExtra(getString(R.string.track_variable)) != null){
            track = (Track)getIntent().getSerializableExtra(getString(R.string.track_variable));
            setupTrackInformation(track);
        }
    }

    // set up media player to play music
    private void setUpMediaPlayer(String previewUrl){
        if(Uri.parse(previewUrl)!= null){
            mediaPlayer = MediaPlayer.create(mContext, Uri.parse(previewUrl));
            mediaPlayer.start();
        }
    }
}

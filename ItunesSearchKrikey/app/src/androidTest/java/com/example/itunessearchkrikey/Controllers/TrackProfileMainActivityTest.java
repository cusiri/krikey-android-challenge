package com.example.itunessearchkrikey.Controllers;

import android.support.test.rule.ActivityTestRule;

import com.example.itunessearchkrikey.Controllers.TrackProfileActivity.TrackProfileMainActivity;
import com.example.itunessearchkrikey.R;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

public class TrackProfileMainActivityTest {

    // Launch Actiivity
    @Rule
    public ActivityTestRule<TrackProfileMainActivity> mainActivityTestActivityTestRule = new ActivityTestRule<>(TrackProfileMainActivity.class);


    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void checkTheTextViewsHaveTheCorrectValues() {
        // Test back button
        onView(withId(R.id.layout_nav_bar_back_button_id)).perform(click());

    }


}
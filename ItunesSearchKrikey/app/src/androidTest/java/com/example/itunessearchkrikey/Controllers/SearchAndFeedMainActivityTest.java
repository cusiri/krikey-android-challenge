package com.example.itunessearchkrikey.Controllers;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;

import com.example.itunessearchkrikey.Controllers.SearchFeedActivity.SearchAndFeedMainActivity;
import com.example.itunessearchkrikey.Controllers.SearchFeedActivity.SearchFeedRecyclerViewAdapter;
import com.example.itunessearchkrikey.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;

import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class SearchAndFeedMainActivityTest {
    private final String textString = "Save me tonight";

    // Launch Activity
    @Rule
    public ActivityTestRule<SearchAndFeedMainActivity> mainActivityActivityTestRule = new ActivityTestRule<>(SearchAndFeedMainActivity.class);

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    // Search For a song using edit text and select song

    @Test
    public void querySongAndSelect() {

        // Enter text to edit text
        onView(withId(R.id.search_feed_activity_main_edit_text_id)).perform(typeText(textString));

        // Click recycler view to close keyboard and segue
        onView(withId(R.id.search_feed_activity_recycler_view_id)).perform(actionOnItemAtPosition(0, click()));

    }

    @Test
    public void testingScrollFunctionAndCClick() {

        // Enter text
        onView(withId(R.id.search_feed_activity_main_edit_text_id)).perform(typeText(textString));

        onView(withId(R.id.search_feed_activity_recycler_view_id)).perform(RecyclerViewActions.<SearchFeedRecyclerViewAdapter.ViewHolder>scrollToPosition(5));

        // Click recycler view to close keyboard and segue
        onView(withId(R.id.search_feed_activity_recycler_view_id)).perform(actionOnItemAtPosition(0, click()));

        // Test Back button
        onView(withId(R.id.layout_nav_bar_back_button_id)).perform(click());

    }
}